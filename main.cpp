#include <iostream>
#include "Complex.h"

int main(int argc, const char * argv[]) {
    Complex a(4.0, 3.0);
    Complex b(3.0, 2.0);
    
    //a.add(b);
    a.multiply(b);
    a.print();
    return 0;
}
