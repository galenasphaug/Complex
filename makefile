CC=g++
CC_PARAMS=-std=c++11 -Wall -pedantic

complex: Complex.o Complex.h main.o
	$(CC) $(CC_PARAMS) Complex.o main.o -o complex
Complex.o: Complex.cpp Complex.h
	$(CC) $(CC_PARAMS) -c Complex.cpp
main.o: main.cpp Complex.h
	$(CC) $(CC_PARAMS) -c main.cpp
run: complex
	./complex
clean:
	rm -rf complex *.o *~ .*~
