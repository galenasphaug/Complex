#include <iostream>
#include <iomanip>
#include <cmath>

#include "Complex.h"

Complex::Complex() {
    // Set all values to 0
    real = 0.0;
    imag = 0.0;
    mag = 0.0;
    angle = 0.0;

    // These are valid coordinates
    polar = true;
    cartesian = true;
}

Complex::Complex(double r, double i/*, bool isPolar*/) {
    real = r;
    imag = i;

    cartesian = true;
    polar = false;
}

void Complex::calculateCartesian() {
    if (cartesian) {
        // Skip if already calculated
        return;
    }
    
    if (polar) {
        // Calculate
        setReal(getMag() * std::cos(getAngle()));
        setImag(getMag() * std::sin(getAngle()));
        cartesian = true;
    } else {
        // Can't calculate without polar coordinates
        std::cout << "calculateCartesian() error: No valid Polar coordinates.\n";
    }
}

void Complex::calculatePolar() {
    if (polar) {
        // Skip if already calculated
        return;
    }
    
    if (cartesian) {
        setMag(std::sqrt(real * real + imag * imag));
        setAngle(std::atan(imag / real));
        polar = true;
    } else {
        std::cout << "calculatePolar() error: No valid Cartesian coordinates.\n";
    }
}

double Complex::getReal() {
    if (!cartesian) {
        // Calculate if needed, thus not making the function const
        calculateCartesian();
    }
    return real;
}

double Complex::getImag() {
    if (!cartesian) {
        calculateCartesian();
    }
    return imag;
}

double Complex::getMag() {
    if (!polar) {
        calculatePolar();
    }
    return mag;
}

double Complex::getAngle() {
    if (!polar) {
        calculatePolar();
    }
    return angle;
}

void Complex::setReal(double r) {
    real = r;
}

void Complex::setImag(double i) {
    imag = i;
}

void Complex::setMag(double m) {
    mag = m;
}

void Complex::setAngle(double a) {
    angle = a;
}

void Complex::add(Complex c) {
    // Verify that we have valid Cartesian coordinates for both numbers
    if (!cartesian) {
        calculateCartesian();
    }
    if (!c.cartesian) {
        c.calculateCartesian();
    }
    // Add real and imaginary parts
    real += c.real;
    imag += c.imag;
    cartesian = true;
    polar = false;
}

void Complex::multiply(Complex c) {
    // Verify that we have valid polar coordinates for both numbers
    if (!polar) {
        calculatePolar();
    }
    if (!c.polar)  {
        c.calculatePolar();
    }
    // Multiply magnitudes, add angles
    mag *= c.mag;
    angle += c.angle;
    cartesian = false;
    polar = true;
}

void Complex::print() {
    std::cout << std::fixed << getReal() << " + " << getImag() << "i\nMagnitude (r): "
              << getMag() << " Angle (\u03B8):" << getAngle() << '\n';
}
