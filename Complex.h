/** @file complex.hpp
 * A class for storing complex numbers, converting between
 * Polar and Cartesian coordinates, and adding and dividing complex numbers.
 *
 * @author Galen Asphaug
 * @version 1.0 12/23/17
**/

class Complex {
private:

    // Flags to check weather current values are valid
    bool polar;
    bool cartesian;

    // Cartesian coordinates
    double real;    // Real
    double imag;    // Imaginary

    // Polar coordinates
    double mag;     // Magnitude
    double angle;   // Angle

    // Convert from Polar to Cartesian and vice versa
    /**
     Calculate Cartesian coordinates from existing valid polar coordinates
     */
    void calculateCartesian();
    
    /**
     Calculate polar coordinates from existing valid Cartesian coordinates
     */
    void calculatePolar();

public:
    // Constructors
    /**
     Construct a new complex number with all values at 0
     */
    Complex();
    
    /**
     Construct a new complex number with set real and imaginary parts
     @param real the real part of the complex number
     @param imag the imaginary part of the complex number
     */
    Complex(double real, double imag);

    // Accessors
    /**
     Get the real part of the complex number, calculating from polar coordinates if nessicary.
     @return the real part of the complex number
     */
    double getReal(); //Not const, these may call calculateX() if values are not valid
    
    /**
     Get the imaginary part of the complex number, calculating from polar coordinates if nessicary.
     @return the imaginary part of the complex number
     */
    double getImag();
    
    /**
     Get the magnitude of the complex number from polar coordinates, calculating from Cartesian coordinates if nessicary.
     @return the magnitude of the polar coordinates of the complex number
     */
    double getMag();
    
    /**
     Get the angle of the complex number from polar coordinates, calculating from Cartesian coordinates if nessicary.
     @return the angle of the polar coordinates of the complex number
     */
    double getAngle();

    // Mutators
    /**
     Set the real part of the complex number
     @param r the real part of the complex number
     */
    void setReal(double r);
    
    /**
     Set the imaginary part of the complex number
     @param i the imaginary part of the complex number
     */
    void setImag(double i);
    
    /**
     Set the magnitude of the complex number
     @param m the magnitude of the complex number
     */
    void setMag(double m);
    
    /**
     Set the angle of the complex number
     @param a the angle of the complex number
     */
    void setAngle(double a);
    
    // Maths
    /**
     Add a complex number to the current complex number
     @param c the complex number to add
     */
    void add(Complex c);
    
    /**
     Multiply the current complex number by another
     @param c the complex number to multiply by
     */
    void multiply(Complex c);

  // TODO: Fix formatting and add operators
  
  Complex operator*=(Complex c);

  Complex operator*(Complex c) const;

  Complex operator+=(Complex c);

  Complex operator+(Complex c) const;

  /**
     Print the complex number to stdout, including the real and imaginary parts,
     as well as the angle and magnitude of the polar coordinates.
     */
    void print();
};
